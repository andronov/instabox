from django.conf.urls import include, url
from django.contrib import admin
from core.views import *

urlpatterns = [
    # Examples:
    #url(r'^$', IndexView.as_view(), name='home'),
    url(r'^$', test, name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^api/v1/test$', testjson, name='tw_login'),
    url(r'^api/v1/users/media$', media_users, name='media_users'),
    url(r'^api/v1/users$', info_users, name='users'),

    url(r'^admin/', include(admin.site.urls)),
]
