# -*- coding: utf-8 -*-
from django.conf import settings
from django.http import JsonResponse
from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView
from instagram import InstagramAPI

api = InstagramAPI(access_token=settings.INSTAGRAM_ACCESS_TOKEN,
                       client_secret=settings.INSTAGRAM_CLIENT_SECRET)

class IndexView(TemplateView):
    template_name = 'base.html'

    def dispatch(self, *args, **kwargs):
        return super(IndexView, self).dispatch(*args, **kwargs)

def test(request):
    api = InstagramAPI(access_token=settings.INSTAGRAM_ACCESS_TOKEN,
                       client_secret=settings.INSTAGRAM_CLIENT_SECRET)

    users = api.user_search(q='barselona', count=2)
    print(users)
    result = []
    for user in users:
        recent_user = api.user(user_id=user.id)
        result.append({recent_user.username: {'id': recent_user.id,
                                        'username': recent_user.username,
                                        'full_name': recent_user.full_name,
                                        'profile_picture': recent_user.profile_picture}})
    print(result)

    return render(request, 'base.html',)
    """
    api = InstagramAPI(access_token=settings.INSTAGRAM_ACCESS_TOKEN,
                       client_secret=settings.INSTAGRAM_CLIENT_SECRET)

    api.tag(tag_name='FCBarcelona')
    recent_media, next_ = api.tag_recent_media(tag_name='FCBarcelona',count=20)
    for media in recent_media:
       print(media.images['standard_resolution'].url)
    """
    """
    users = api.user_search(q='andronov04')
    recent_media, next_ = api.user_recent_media(user_id=users[0].id)
    for media in recent_media:
       print(media.images['standard_resolution'].url)
    """

def info_users(request):
    data = []
    recent_user = api.user(user_id=request.GET['user'])
    data.append({'id': recent_user.id,
                 'username': recent_user.username,
                 'full_name': recent_user.full_name,
                 'profile_picture': recent_user.profile_picture,
                 'followed_by': recent_user.counts['followed_by'],
                 'follows': recent_user.counts['follows'],
                 'media': recent_user.counts['media']})

    return JsonResponse(data, safe=False)


def media_users(request):
    data = []

    recent_media = api.user_recent_media(user_id=request.GET['user'])
    try:
      for media in recent_media:
        for md in media:
            #print(md.location)
            data.append({'id': md.id,
                        'url': md.images['standard_resolution'].url,
                        'created_time': md.created_time.strftime("%d.%m.%y"),
                        'like_count': md.like_count,
                        'signature': md.caption.text})
    except:
        pass

    
    #for media in recent_media:
    #    data.append({
    #                 'url': media.images['standard_resolution'].url})

    return JsonResponse(data, safe=False)

    #users = api.user_search(q=request.GET['search'],count=5)
    #print(users)
    #data = {'foo': 'bar'}
    """
    users = api.user_search(q=request.GET['search'], count=2)
    print(users)
    data = []
    for user in users:
      try:
        recent_user = api.user(user_id=user.id)
        data.append({'id': recent_user.id,
                                        'username': recent_user.username,
                                        'full_name': recent_user.full_name,
                                        'profile_picture': recent_user.profile_picture})
      except:
         pass
    return JsonResponse(data, safe=False)
    """


def testjson(request):

    #users = api.user_search(q=request.GET['search'],count=5)
    #print(users)
    #data = {'foo': 'bar'}
    users = api.user_search(q=request.GET['search'], count=2)
    print(users)
    data = []
    for user in users:
      try:
        recent_user = api.user(user_id=user.id)
        data.append({'id': recent_user.id,
                                        'username': recent_user.username,
                                        'full_name': recent_user.full_name,
                                        'profile_picture': recent_user.profile_picture})
      except:
         pass
    return JsonResponse(data, safe=False)